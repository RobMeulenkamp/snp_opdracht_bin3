#! usr/bin/env python3

""""
Predicts the severity of the mutation
"""

__author__ = "Rob Meulenkamp"
__status__ = "Development"
__version__ = "v0.1"


from Bio import AlignIO, Align
import argparse
import sys
from Bio import Seq


class SnpPredictor:
    def __init__(self):
        self.dna_seq = ""
        self.aa_seq = ""

    def read_file(self, file, msa):
        """Reads the file"""
        valid = "ACGT"
        valid_char = ["A", "C", "G", "T"]
        if msa.endswith("msf"):
            alignment = AlignIO.read(open(msa), "msf")
        else:
            raise ValueError("File should be in .msf format")

        with open(file) as files:
            for line in files:
                line = line.strip()
                if line not in valid_char:
                    self.dna_seq += line.upper()
                    
                else:
                    raise ValueError("Oeps... invalid value found. "
                                           "please check the file "
                                           "if it only contains DNA nucleotides")             
                        
        return alignment

    def aa_translater(self):
        """ Translate DNA seq --> Amino Acid seq """

        if len(self.dna_seq) % 3 == 0:
            self.aa_seq = Seq.translate(self.dna_seq, stop_symbol="-")

        elif len(self.dna_seq) % 3 == 1:
            self.dna_seq = self.dna_seq[:-1]
            self.aa_seq = Seq.translate(self.dna_seq, stop_symbol="-")

        elif len(self.dna_seq) % 3 == 2:
            self.dna_seq = self.dna_seq[:-2]
            self.aa_seq = Seq.translate(self.dna_seq, stop_symbol="-")
            
        return 0

    def snp_impl(self, snp, pos):
        """Implements SNP at given position"""
        if pos < 0 or pos > len(self.dna_seq):
            raise IndexError("Position doesn't exist, Try another position!")
        else:
            self.dna_seq = self.dna_seq[:pos] + snp.upper() + self.dna_seq[pos + 1:]

        return 0

    def alignment_score(self, alignment):
        """"gives the alignment a score"""
        length = len(alignment)
        align_scores = []
        aligner = Align.PairwiseAligner()
        
        for item in range(0, len(self.aa_seq)):
            alignments = aligner.align(alignment[:, item],
                                       self.aa_seq[item] * length)
            align_scores.append(alignments.score)
        return align_scores, length

    def snp_effector(self, score, pos, length):
        """
        Prints the result to the user
        """

        message = "Your score isn't right"
        if score[pos] <= 0.15 * length:
            message = "The SNP creates a bad effect.\n" \
                      "The amino acid might be preserved!"
        elif 0.15 * length < score[pos] <= 0.45 * length:
            message = "The SNP has some bad affects, but is isn't the end of the world"
        elif 0.45 * length < score[pos] <= 0.75 * length:
            message = "So the SNP has little to no effect."
        elif score[pos] > 0.75 * length:
            message = "SNP has neutral effect"
        print(f"Effect of SNP at pos: {pos}, has score:"
              f" {score[pos]} / {length}.\n{message}")


def main(args):
    """main function"""
    parser = argparse.ArgumentParser(description='Process a MSA.')

    parser.add_argument('-i', type=str, dest="msf", required=True,
                        help="Name of a msf file which contains a MSA")

    parser.add_argument('-seq', type=str, dest="seq", required=True,
                        help="Please give DNA sequence file")

    parser.add_argument('-pos', dest="pos", required=True, type=int,
                        help="position SNP and overwrite original amino acid")

    parser.add_argument('-snp', dest="snp", type=str, required=True, choices={"A", "C", "G", "T"},
                        help="""SNP to predict the score""")

    args = parser.parse_args()
    pos = args.pos
    snp = args.snp
    seq = args.seq
    msf = args.msf
    
    snp_object = SnpPredictor()
    alignment = snp_object.read_file(seq, msf)
    snp_object.snp_impl(snp, pos)
    snp_object.aa_translater()
    result, length = snp_object.alignment_score(alignment)
    snp_object.snp_effector(result, pos, length)

    return 0


if __name__ == "__main__":
    exit_code = main(sys.argv)
    sys.exit(exit_code)
