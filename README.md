# README #

Author: Rob Meulenkamp
Date: 05-11-2021

Helaas niet gelukt om het te laten runnen op command prompt
Het programma doet wel als je in pycharm het programma runt.

Link naar resultaat in python:
https://gyazo.com/2affc7af61c7a792c488d72ebfee8251


Test data is meegeleverd!
msa2.msf
gene.txt

-i = input MSA file
-seq = DNA sequentie
-pos = positie waar de snp komt
-snp = de SNP zoals A, C, G of T.

Als het programma werkt zou u dit moeten invoeren in de command:

python3 snp_opdr.py -i msa2.msf -seq gene.txt -pos 10 -snp "A"


Support:

Voor vragen mail naar:
l.r.meulenkamp@st.hanze.nl